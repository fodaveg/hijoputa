<?php

$host = $_SERVER['HTTP_HOST'];

$host = idn_to_utf8($host);

$subDomain = str_replace('.hijoputa.es','',$host);
$subDomain = mb_strtoupper(str_replace('-',' ',$subDomain));
$text = $subDomain. " HIJOPUTA ES";

$servername = "localhost";
$username = "hijoputa";
$password = "hijoputa.123";
$dbname = "hijoputa";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO hijoputa_visits (name)
VALUES ('$subDomain')";

if (!$conn->query($sql) === TRUE) {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>

<html>

    <head>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src='https://code.responsivevoice.org/responsivevoice.js'></script>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <title><?php echo $text; ?></title>
    </head>

    <body class="clickable">
        <div class="centered hijoputa">
            <h1><?php echo $text; ?></h1>
        </div>

        <script type="text/javascript">
            var val = "<?php echo $text; ?>";
            $(document).ready(function()
            {

                responsiveVoice.speak(val, "Spanish Female", {rate: 1, pitch: -2, volume: 2});
                $(".clickable").click(function()
                {
                    responsiveVoice.speak(val, "Spanish Female", {rate: 1, pitch: -2, volume: 2});
                });

            });

        </script>
    </body>
</html>
